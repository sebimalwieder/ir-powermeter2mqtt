#!/usr/bin/env python3

# TCRT5000 tracking sensor
import paho.mqtt.client as mqtt
from gpiozero import LineSensor
import signal, sys
import time
import yaml
import json


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


TOPIC = "homeassistant/powermeter"
BROKER_ADDRESS = "sebserver"
STATE_FILE = "/opt/power/state.yaml"
PORT = 1883
QOS = 1
PIN = 4
SENSOR = LineSensor(PIN)
TIME_IS_TICKING = False
LAST_TIME_STAMP = 0
IMPULSES_PER_KWH = 75
CURRENT_TOTAL_VALUE = 0.0
AD_PAYLOAD_CURRENT_CONSUMPTION = {
    "state_topic": "homeassistant/powermeter/power",
    "state_class": "measurement",
    "device_class": "power",
    "icon": "mdi:flash",
    "name": "Aktueller Gesamtleistung",
    "availability_topic": "homeassistant/powermeter/availability",
    "unit_of_measurement": "W",
    "unique_id": "powermeter-000001-power",
    "value_template": "{{ (value | float) | round(2) }}",
    "device": {"identifiers": "PiPowermeter-000001", "name": "Powermeter", "model": "TCRT5000", "manufacturer": "Sebastian"}
}
AD_PAYLOAD_TOTAL_CONSUMPTION = {
    "state_topic": "homeassistant/powermeter/energy",
    "state_class": "total_increasing",
    "device_class": "energy",
    "icon": "mdi:counter",
    "name": "Aktueller Zählerstand",
    "availability_topic": "homeassistant/powermeter/availability",
    "unit_of_measurement": "kWh",
    "unique_id": "powermeter-000001-energy",
    "value_template": "{{ (value | float) | round(5) }}",
    "device": {"identifiers": "PiPowermeter-000001", "name": "Powermeter", "model": "TCRT5000", "manufacturer": "Sebastian"}
}
client = mqtt.Client()


def setup():
    signal.signal(signal.SIGINT, signal_handler)
    loadState()
    print("Now measuring...")


# reading last value from state.yaml
def loadState():
    global CURRENT_TOTAL_VALUE
    with open(STATE_FILE) as stream:
        data = yaml.load(stream, Loader=yaml.loader.SafeLoader)
        CURRENT_TOTAL_VALUE = data['lastState']['value']
        lastUpdate = time.strftime(
            '%Y-%m-%d %H:%M:%S', time.localtime(data['lastState']['timestamp']))
        print(bcolors.OKCYAN + "Loaded last state: " + bcolors.ENDC + str(CURRENT_TOTAL_VALUE) + "KWh\n" +
              bcolors.OKCYAN + "Last update: " + bcolors.ENDC + lastUpdate)


# saving last value to state.yaml
def saveState():
    with open(STATE_FILE, 'w') as outfile:
        yaml.dump({'lastState': {'value': CURRENT_TOTAL_VALUE,
                  'timestamp': LAST_TIME_STAMP}}, outfile, default_flow_style=False)


def loop():
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.connect(BROKER_ADDRESS, PORT, 600)
    SENSOR.when_line = updateState
    client.loop_forever()

def on_connect(client, userdata, flags, rc):
    print("[DEBUG] Connected with result code " + str(rc))
    print("Connected to MQTT Broker: " + BROKER_ADDRESS)
    client.publish(
        "homeassistant/sensor/000001/powermeter-power/config", json.dumps(AD_PAYLOAD_CURRENT_CONSUMPTION), qos=QOS)
    client.publish(
        "homeassistant/sensor/000001/powermeter-energy/config", json.dumps(AD_PAYLOAD_TOTAL_CONSUMPTION), qos=QOS)
    client.publish(TOPIC + "/availability", "online", qos=QOS)

def on_disconnect(client, userdata, rc):
    print("[DEBUG] Disconnected with result code " + str(rc))
    print("Disconnected from MQTT Broker: " + BROKER_ADDRESS)
    print("Trying to reconnect ...")

def updateTotalValue():
    global CURRENT_TOTAL_VALUE
    CURRENT_TOTAL_VALUE += float(1/IMPULSES_PER_KWH)
    client.publish(TOPIC + '/energy', CURRENT_TOTAL_VALUE, qos=QOS)
    saveState()
    print(bcolors.OKGREEN + "💸 Total Value: " +
          str(CURRENT_TOTAL_VALUE) + "KWh" + bcolors.ENDC + "\n")


def updateCurrentConsumption(seconds):
    print("Last interval: " + str(seconds))
    currentPowerConsumption = 3600000 / (seconds * IMPULSES_PER_KWH)
    print(bcolors.OKGREEN + "⚡ Current Power Consumption: " +
          str(currentPowerConsumption) + "W" + bcolors.ENDC)
    client.publish(TOPIC + '/power', currentPowerConsumption, qos=QOS)


def updateState():
    global TIME_IS_TICKING, LAST_TIME_STAMP, CURRENT_TOTAL_VALUE
    print("Caught sensor reading.")
    client.publish(TOPIC + "/availability", "online", qos=QOS)
    if (TIME_IS_TICKING):
        timeDiff = time.time() - LAST_TIME_STAMP
        LAST_TIME_STAMP = time.time()
        updateCurrentConsumption(timeDiff)
        updateTotalValue()
    else:
        TIME_IS_TICKING = True
        LAST_TIME_STAMP = time.time()
        print("🏃 First run. Now measuring seconds between two impulses.")
        updateTotalValue()


def destroy():
    SENSOR.close()
    client.publish(TOPIC+"/availability", "offline", qos=QOS)
    client.loop_stop()  # Stop loop 
    client.disconnect() # disconnect

def signal_handler(sig, frame):
    print('[INFO] Got SIGINT. Stopping ...')
    destroy()
    sys.exit(0)


if __name__ == '__main__':
    setup()
try:
    loop()
except KeyboardInterrupt:
    print("[INFO] Ctrl+C pressed. Stopping ...")
    destroy()
except Exception as e:
    print("[ERROR] Exception caught: " + str(e))
    destroy()
