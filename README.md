# powermeter2mqtt

This little deamon will publish power and energy sensor readings to home assistant via an MQTT broker for digitalizing a old power meter (like Ferraris, ...).
Build with python3. Supports auto discovery for Home Assistant.

## Installation

Will be added later.

## Hardware

- Raspberry Pi Zero W (MCUs would be better, but a still had one lying around.)
- TCRT5000, as seen [here](https://www.ebay.de/itm/323964077540?mkevt=1&mkpid=0&emsid=e11050.m43.l1123&mkcid=7&ch=osgood&euid=d182babc184e49f790b3f200204d9000&bu=45291109807&osub=-1%7E1&crd=20221128051251&segname=11050)
- Female to female jumper wires

## Credits & props 💖

- [Paho MQTT](https://pypi.org/project/paho-mqtt/)
- [gpiozero](https://gpiozero.readthedocs.io/en/stable/)
